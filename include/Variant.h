#pragma once
#include <Arduino.h>

enum class SensorDataType : uint8_t {
    Temperature = 0,
    Pressure = 1,
    Humidity = 2,
    Co2 = 3,
    Light = 4
};

enum class ValueType : uint8_t {
    BOOL = 0,
    FLOAT = 1,
    INT8_T = 2,
    INT16_T = 3,
    INT32_T = 4,
    UINT8_T = 5,
    UINT16_T = 6,
    UINT32_T = 7
};

struct Variant {
    union Value {
        bool bool_value;
        float float_value;
        int8_t int8_value;
        int16_t int16_value;
        int32_t int32_value;
        uint8_t uint8_value;
        uint16_t uint16_value;
        uint32_t uint32_value;
    };
    Variant(ValueType vType, SensorDataType vTypeOfSensorData)
        : typeOfValue(vType), typeOfSensorData(vTypeOfSensorData){};

    void setValue(bool value_new) { value.bool_value = value_new; };
    void setValue(float value_new) { value.float_value = value_new; }
    void setValue(int8_t value_new) { value.int8_value = value_new; }
    void setValue(int16_t value_new) { value.int16_value = value_new; }
    void setValue(int32_t value_new) { value.int32_value = value_new; }
    void setValue(uint8_t value_new) { value.uint8_value = value_new; }
    void setValue(uint16_t value_new) { value.uint16_value = value_new; }
    void setValue(uint32_t value_new) { value.uint32_value = value_new; }

    template <typename T>
    const T getValue() const {
        switch (typeOfValue) {
            case ValueType::BOOL:
                return value.bool_value;
                break;
            case ValueType::FLOAT:
                return value.float_value;
                break;
            case ValueType::INT8_T:
                return value.int8_value;
                break;
            case ValueType::INT16_T:
                return value.int16_value;
                break;
            case ValueType::INT32_T:
                return value.int32_value;
                break;
            case ValueType::UINT8_T:
                return value.uint8_value;
                break;
            case ValueType::UINT16_T:
                return value.uint16_value;
                break;
            case ValueType::UINT32_T:
                return value.uint32_value;
                break;
            default:
                static_assert("ERROR get value from Variant");
                break;
        }
        static_assert("ERROR get value from Variant");
        return 0;
    };
    ValueType typeOfValue;
    SensorDataType typeOfSensorData;

   private:
    Value value;
};