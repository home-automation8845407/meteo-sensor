#pragma once
#include "ManagerSensor.h"

class ManagerSerial {
   private:
    ManagerSensor *data_ = nullptr;
    void SendDebugDataToSerialPort();

   public:
    ManagerSerial();
    ~ManagerSerial();
    void AddManagerSensor(ManagerSensor *data) { data_ = data; }
    void Update(bool debug = false);
    // void printData(Variant var);
};
