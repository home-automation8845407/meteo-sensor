#include "ManagerSerial.h"

ManagerSerial::ManagerSerial() {
    Serial.begin(9600);
    Serial.println("Start");
}

void ManagerSerial::SendDebugDataToSerialPort() {
    if (data_) {
        for (int i = 0; i < data_->GetDataFromSensors().getSize(); ++i) {
            Serial.print(
                static_cast<uint8_t>(data_->GetDataFromSensors().getValue(i).typeOfSensorData));
            Serial.print(" - ");
            Serial.print(data_->GetDataFromSensors().getValue(i).getValue<float>());
            Serial.println("   ");
        }
    }
    Serial.println("");
}

void ManagerSerial::Update(bool debug) {
    if (debug) {
        SendDebugDataToSerialPort();
    }
};
